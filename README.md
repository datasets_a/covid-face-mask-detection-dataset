# COVID Face Mask Detection Dataset
This dataset contains about 1006 equally distributed images of 2 distinct types.

Source: https://www.kaggle.com/prithwirajmitra/covid-face-mask-detection-dataset

Useful links:

1. https://www.cs.ryerson.ca/~aharley/vis/conv/flat.html